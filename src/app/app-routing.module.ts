import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LayoutComponent} from './core/layout/layout.component';

const routes: Routes = [{
  path: '', component: LayoutComponent,
  //  con children definisco array di rotte figlie di layout
  children: [
    {
      path: 'home',
      loadChildren: () => import('./features/home/home.module').then(m => m.HomeModule)
    },
    {
      path: 'search',
      loadChildren: () => import('./features/search/search.module').then(m => m.SearchModule)
    },
    {
      path: 'login',
      loadChildren: () => import('./features/login/login.module').then(m => m.LoginModule)
    },
    {
      path: 'signup',
      loadChildren: () => import('./features/signup/signup.module').then(m => m.SignupModule)
    }/*,
    {
      path: 'detail',
      loadChildren: () => import('./features/detail/detail.module').then(m => m.DetailModule)
    }*/
  ]
},

  // componente da caricare di default se non è specificato un path
  { path: '', redirectTo: 'home', pathMatch: 'full'},
  { path: '**', redirectTo: 'home', pathMatch: 'full'}
  // componente da caricare se il path scritto non esiste
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
